import numpy as np

from loaders.preprocess.preprocessor import Preprocessor


class CenterAndNormalize(object, Preprocessor):

    def __init__(self):
        Preprocessor.__init__(self)
        self._xmean = None
        self._xstd = None

    def reset(self):
        self._xmean = None
        self._xstd = None

    def initialize(self, xtrain):
        self._xmean = np.mean(xtrain, axis=0, keepdims=True)
        self._xstd = np.std(xtrain, axis=0, keepdims=True)

    def process(self, x):
        # type: (np.ndarray) -> np.ndarray
        x_centered = x - self._xmean
        x_normalized = x_centered / self._xstd
        return x_normalized
