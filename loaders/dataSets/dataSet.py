import numpy as np

from loaders.dataSets.batch import Batch


class DataSet:

    def __init__(self):
        self._x = None
        self._y = None
        self._epochs_completed = 0
        self._index_in_epoch = 0

    @property
    def x(self):
        # type: () -> np.ndarray
        return self._x

    @property
    def y(self):
        # type: () -> np.ndarray
        return self._y

    @property
    def num_examples(self):
        # type: () -> int
        return self._x.shape[0]

    @property
    def epochs_completed(self):
        # type: () -> int
        return self._epochs_completed

    def next_batch(self, size, shuffle=True):
        # type: (int, bool) -> (np.ndarray, np.ndarray)
        pass
