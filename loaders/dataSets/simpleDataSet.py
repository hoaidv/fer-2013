import numpy as np

from loaders.augment.augmentor import Augmentor
from loaders.dataSets.batch import Batch
from loaders.dataSets.dataSet import DataSet
from loaders.ferLoader import validate_input, load_csv, load_numpy
from loaders.preprocess.preprocessor import Preprocessor


class SimpleDataSet(DataSet):

    def __init__(self, x, y):
        DataSet.__init__(self)
        numpy_x, numpy_y = validate_input(x, y)
        self._x = numpy_x
        self._y = numpy_y

    @classmethod
    def from_csv(cls, file_path):
        numpy_x, numpy_y = load_csv(file_path=file_path)
        return SimpleDataSet(numpy_x, numpy_y)

    @classmethod
    def from_numpy(cls, folder, basename=''):
        # type: (str, str, Augmentor, Preprocessor) -> SimpleDataSet
        x, y = load_numpy(folder=folder, basename=basename)
        return SimpleDataSet(x, y)

    def next_batch(self, size, shuffle=True):
        # type: (int, bool) -> (np.ndarray, np.ndarray)
        start = self._index_in_epoch
        if self._epochs_completed == 0 and start == 0 and shuffle:
            self._shuffle()

        if start + size > self.num_examples:
            self._epochs_completed += 1
            rest_num_examples = self.num_examples - start
            x_rest_part = self._x[start:self.num_examples]
            y_rest_part = self._y[start:self.num_examples]

            if shuffle:
                self._shuffle()

            start = 0
            end = self._index_in_epoch = size - rest_num_examples
            x_new_part = self._x[start:end]
            y_new_part = self._y[start:end]
            batch_x = np.concatenate((x_rest_part, x_new_part), axis=0)
            batch_y = np.concatenate((y_rest_part, y_new_part), axis=0)

        else:
            self._index_in_epoch += size
            end = self._index_in_epoch
            batch_x = self._x[start:end]
            batch_y = self._y[start:end]

        return batch_x, batch_y

    def _shuffle(self):
        permutation = np.arange(self.num_examples)
        np.random.shuffle(permutation)
        self._x = self._x[permutation]
        self._y = self._y[permutation]
