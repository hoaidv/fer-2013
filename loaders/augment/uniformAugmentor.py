from loaders.augment.augmentor import Augmentor
from loaders.augment.batchProcess import *
import batchProcess as bp
import numpy as np
import math

from util import plot


class UniformAugmentor(Augmentor):

    def __init__(self):
        Augmentor.__init__(self)

    def augment(self, x, y, preview=False):
        # type: (np.ndarray, np.ndarray) -> (np.ndarray, np.ndarray)

        flipped_imgs, flipped_idx = bp.flip_horizontal(x, 0.5)
        mix_imgs = np.copy(x)
        mix_imgs[flipped_idx] = flipped_imgs
        rotated = bp.rotate(mix_imgs, -45, 45)
        rescaled = bp.rescale(rotated, 1, math.sqrt(2))

        if preview:
            self._preview(rescaled)

        return rescaled, y

    @staticmethod
    def _preview(aug_x):
        aug_x = np.array(aug_x)
        all_idxs = range(len(aug_x))
        for i in xrange(5):
            sample_idxs = random.sample(population=all_idxs, k=100)
            sample_imgs = aug_x[sample_idxs]
            plot.show_images(sample_imgs)
            raw_input('Press enter to continue...')

    '''
    def augment(self, x, y, preview=False):
        # type: (np.ndarray, np.ndarray) -> (np.ndarray, np.ndarray)
        aug_x, aug_y = [], []

        rescaled = bp.rescale(x, 1/1.6, 1.6)
        aug_x.append(rescaled)
        aug_y.append(y)

        translated = bp.translate(x, (-8, 8), (-8, 8))
        aug_x.append(translated)
        aug_y.append(y)

        rotated = bp.rotate(x, -30, 30)
        aug_x.append(rotated)
        aug_y.append(y)

        flipped, flipped_idx = bp.flip_horizontal(x, 0.3)
        aug_x.append(flipped)
        aug_y.append(y[flipped_idx])

        sheared_hor = bp.shear_horizontal(x, -30, 30)
        aug_x.append(sheared_hor)
        aug_y.append(y)

        sheared_ver = bp.shear_vertical(x, -30, 30)
        aug_x.append(sheared_ver)
        aug_y.append(y)

        gaussian_blurred = bp.gaussian_blur(x, 0.1, 2.5)
        aug_x.append(gaussian_blurred)
        aug_y.append(y)

        median_blurred = bp.median_blur(x, 1, 3)
        aug_x.append(median_blurred)
        aug_y.append(y)

        noised = bp.gaussian_noise_float(x, 1.0/255, 32.0/255)
        aug_x.append(noised)
        aug_y.append(y)

        dropped = bp.dropout(x, 0.05, 0.25)
        aug_x.append(dropped)
        aug_y.append(y)

        added = bp.add_float(x, -32.0/255, 32.0/255)
        aug_x.append(added)
        aug_y.append(y)

        multiplied = bp.multiply_float(x, 0.5, 1.5)
        aug_x.append(multiplied)
        aug_y.append(y)

        aug_x = np.concatenate(aug_x, axis=0)
        aug_y = np.concatenate(aug_y, axis=0)

        if preview: self._preview(aug_x)

        return aug_x, aug_y
    '''
