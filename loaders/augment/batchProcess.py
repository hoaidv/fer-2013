import math
import random
import multiprocessing
import numpy as np
from numpy.random import uniform, randint
from joblib import Parallel, delayed

from loaders.augment import singleProcess as sp


parallel_jobs = multiprocessing.cpu_count()
_work = Parallel(n_jobs=parallel_jobs)


def rescale(images, min_scale, max_scale):
    # type: (np.ndarray, float, float) -> list
    ratio_array = uniform(min_scale, max_scale, len(images))
    return _work(delayed(sp.rescale)(img, ratio) for img, ratio in zip(images, ratio_array))


def translate(images, tx_range=(), ty_range=()):
    # type: (np.ndarray, tuple, tuple) -> list
    min_tx, max_tx = tx_range[0], tx_range[1]
    min_ty, max_ty = ty_range[0], ty_range[1]
    tx_array = randint(min_tx, max_tx + 1, len(images), np.int8)
    ty_array = randint(min_ty, max_ty + 1, len(images), np.int8)
    return _work(delayed(sp.translate)(img, tx, ty) for img, tx, ty in zip(images, tx_array, ty_array))


def rotate(images, min_deg, max_deg):
    deg_array = uniform(min_deg, max_deg, len(images))
    return _work(delayed(sp.rotate)(img, deg) for img, deg in zip(images, deg_array))


def flip_horizontal(images, prob):
    # type: (np.ndarray, float) -> (list, list)
    num_imgs = len(images)
    num_flips = int(num_imgs * prob)
    all_idxs = range(num_imgs)
    flip_idxs = random.sample(population=all_idxs, k=num_flips)
    flip_imgs = _work(delayed(sp.flip_horizontal)(img) for img in images[flip_idxs])
    return flip_imgs, flip_idxs


def flip_vertical(images, prob):
    # type: (np.ndarray, float) -> (list, list)
    num_imgs = len(images)
    num_flips = int(num_imgs * prob)
    flip_idxs = random.sample(range(num_imgs), num_flips)
    flip_imgs = _work(delayed(sp.flip_vertical)(img) for img in images[flip_idxs])
    return flip_imgs, flip_idxs


def shear_horizontal(images, min_deg, max_deg):
    # type: (np.ndarray, float, float) -> list
    min_rad = min_deg * math.pi / 180
    max_rad = max_deg * math.pi / 180
    rad_array = uniform(min_rad, max_rad, len(images))
    return _work(delayed(sp.shear_horizontal)(img, rad) for img, rad in zip(images, rad_array))


def shear_vertical(images, min_deg, max_deg):
    # type: (np.ndarray, float, float) -> list
    min_rad = min_deg * math.pi / 180
    max_rad = max_deg * math.pi / 180
    rad_array = uniform(min_rad, max_rad, len(images))
    return _work(delayed(sp.shear_vertical)(img, rad) for img, rad in zip(images, rad_array))


def gaussian_blur(images, min_stddev, max_stddev):
    # type: (np.ndarray, object, object) -> list
    num_imgs = len(images)
    img_dims = len(images[0].shape)
    std_array = uniform(min_stddev, max_stddev, size=(num_imgs, img_dims))
    return _work(delayed(sp.gaussian_blur)(img, std) for img, std in zip(images, std_array))


def median_blur(images, min_kernel_size, max_kernel_size):
    # type: (np.ndarray, int, int) -> list
    ksize_array = randint(min_kernel_size, max_kernel_size + 1, len(images))
    return _work(delayed(sp.median_blur)(img, ksize) for img, ksize in zip(images, ksize_array))


def gaussian_noise_float(images, min_level, max_level):
    # type: (np.ndarray, float, float) -> list
    lvl_array = uniform(min_level, max_level, size=len(images))
    return _work(delayed(sp.gaussian_noise_float)(img, lvl) for img, lvl in zip(images, lvl_array))


def gaussian_noise_int(images, min_level, max_level):
    # type: (np.ndarray, float, float) -> list
    lvl_array = uniform(min_level, max_level, size=len(images))
    return _work(delayed(sp.gaussian_noise_int)(img, lvl) for img, lvl in zip(images, lvl_array))


def dropout(images, min_prob, max_prob):
    # type: (np.ndarray, float, float) -> list
    prob_array = uniform(min_prob, max_prob, size=len(images))
    return _work(delayed(sp.dropout)(img, prob) for img, prob in zip(images, prob_array))


def coarse_dropout(images, prob_range=(), size_range=()):
    # type: (np.ndarray, tuple, tuple) -> list
    num_imgs = len(images)
    prob_array = uniform(prob_range[0], prob_range[1], size=num_imgs)
    size_array = randint(size_range[0], size_range[1] + 1, size=num_imgs)
    return _work(delayed(sp.coarse_dropout)(img, prob, size) for img, prob, size in zip(images, prob_array, size_array))


def add_float(images, min_val, max_val):
    # type: (np.ndarray, float, float) -> list
    amt_array = uniform(min_val, max_val, size=len(images))
    return _work(delayed(sp.add_float)(img, amt) for img, amt in zip(images, amt_array))


def add_int(images, min_val, max_val):
    # type: (np.ndarray,  int, int) -> list
    amt_array = randint(min_val, max_val + 1, size=len(images))
    return _work(delayed(sp.add_int)(img, amt) for img, amt in zip(images, amt_array))


def multiply_float(images, min_val, max_val):
    # type: (np.ndarray, float, float) -> list
    amt_array = uniform(min_val, max_val, size=len(images))
    return _work(delayed(sp.multiply_float)(img, amt) for img, amt in zip(images, amt_array))


def multiply_int(images, min_val, max_val):
    # type: (np.ndarray,  float, float) -> list
    amt_array = uniform(min_val, max_val, size=len(images))
    return _work(delayed(sp.multiply_int)(img, amt) for img, amt in zip(images, amt_array))
