import math

import numpy as np
from skimage import transform as transform, filters as filters, morphology as morphology


def rescale(image, ratio):
    # type: (np.ndarray, float) -> np.ndarray
    temp = transform.rescale(image, ratio, mode='reflect')

    if temp.shape[0] < image.shape[0]:
        ox = (image.shape[0] - temp.shape[0]) / 2
        oy = (image.shape[1] - temp.shape[1]) / 2
        scaled_image = np.zeros(image.shape, dtype=temp.dtype)
        scaled_image[ox:ox + temp.shape[0], oy:oy + temp.shape[1]] = temp
    else:
        ox = (temp.shape[0] - image.shape[0]) / 2
        oy = (temp.shape[1] - image.shape[1]) / 2
        scaled_image = temp[ox:ox + image.shape[0], oy:oy + image.shape[1]]
    return scaled_image


def translate(image, tx, ty):
    # type: (np.ndarray, float, float) -> np.ndarray
    translate_transform = transform.AffineTransform(translation=(tx, ty))
    translated_image = transform.warp(image, translate_transform)
    return translated_image


def rotate(image, angle):
    # type: (np.ndarray, float) -> np.ndarray
    rotated_image = transform.rotate(image, angle)
    return rotated_image


def flip_horizontal(image):
    # type: (np.ndarray) -> np.ndarray
    flipped_image = np.zeros_like(image)
    flipped_image[:image.shape[0], image.shape[1] - 1::-1] = image
    return flipped_image


def flip_vertical(image):
    # type: (np.ndarray) -> np.ndarray
    flipped_image = np.zeros_like(image)
    flipped_image[image.shape[0] - 1::-1, :image.shape[1]] = image
    return flipped_image


def shear_horizontal(image, radian):
    # type: (np.ndarray, float) -> np.ndarray
    tx = math.tan(radian) * image.shape[0] / 2
    shear_transform = transform.AffineTransform(shear=radian, translation=(tx, 0))
    sheared_image = transform.warp(image, shear_transform)
    return sheared_image


def shear_vertical(image, radian):
    # type: (np.ndarray, float) -> np.ndarray
    ty = -math.tan(radian) * image.shape[1] / 2
    shear_transform = transform.AffineTransform(
        matrix=np.array([
            [1, 0, 0],
            [math.sin(radian), 1, ty],
            [0, 0, 1],
        ]))
    sheared_image = transform.warp(image, shear_transform)
    return sheared_image


def gaussian_blur(image, std_dev):
    # type: (np.ndarray, list) -> np.ndarray
    blurred_image = filters.gaussian(image, sigma=std_dev)
    return blurred_image


def median_blur(image, kernel_size):
    # type: (np.ndarray, int) -> np.ndarray
    kernel = morphology.disk(kernel_size)
    blurred_image = filters.median(image, kernel)
    return blurred_image


def gaussian_noise_float(image, level):
    # type: (np.ndarray, float) -> np.ndarray
    noisy_layer = np.random.randn(image.shape[0], image.shape[1])
    noisy_layer *= level
    noisy_image = image + noisy_layer
    noisy_image = np.clip(noisy_image, 0, 1)
    return noisy_image


def gaussian_noise_int(image, level):
    # type: (np.ndarray, float) -> np.ndarray
    level = int(level * 255.0)
    noisy_layer = np.random.randint(-level, level, image.shape[0] * image.shape[1], dtype=np.int16)
    noisy_layer = noisy_layer.reshape((image.shape[0], image.shape[1]))
    noisy_image = image + noisy_layer
    noisy_image = np.clip(noisy_image, 0, 255)
    noisy_image = noisy_image.astype(np.uint8)
    return noisy_image


def dropout(image, prob):
    # type: (np.ndarray, float) -> np.ndarray
    mask = 1 * (np.random.rand(image.shape[0], image.shape[1]) > prob)
    dropped_image = image * mask
    return dropped_image


def coarse_dropout(image, prob, drop_size, smooth=False):
    # type: (np.ndarray, float, int, bool) -> np.ndarray
    size_percent = 1.0 / drop_size
    down_width = int(image.shape[0] * size_percent)
    down_height = int(image.shape[1] * size_percent)

    order = 1 if smooth else 0
    downscale_mask = np.random.rand(down_width, down_height) > prob
    originscale_mask = transform.resize(downscale_mask, image.shape, order=order)

    dropped_image = image * originscale_mask
    return dropped_image


def add_float(image, val):
    # type: (np.ndarray, float) -> np.ndarray
    added_image = image.astype(np.float32) + val
    added_image = np.clip(added_image, 0, 1)
    added_image = added_image.astype(image.dtype)
    return added_image


def add_int(image, val):
    # type: (np.ndarray, int) -> np.ndarray
    added_image = image.astype(np.int16) + val
    added_image = np.clip(added_image, 0, 255)
    added_image = added_image.astype(image.dtype)
    return added_image


def multiply_float(image, val):
    # type: (np.ndarray, float) -> np.ndarray
    multiplied_image = image.astype(np.float32) * val
    multiplied_image = np.clip(multiplied_image, 0, 1)
    multiplied_image = multiplied_image.astype(image.dtype)
    return multiplied_image


def multiply_int(image, val):
    # type: (np.ndarray, float) -> np.ndarray
    multiplied_image = image.astype(np.int16) * val
    multiplied_image = np.clip(multiplied_image, 0, 255)
    multiplied_image = multiplied_image.astype(image.dtype)
    return multiplied_image
