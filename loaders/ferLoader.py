
import glob
import os
import yaml

import numpy as np

'''
Data-set contains samples for different purposes:
    Training - 28,709 examples
    PublicTest - 3,589
    PrivateTest - 3,589

Emotions:
    0=Angry, 1=Disgust, 2=Fear, 3=Happy, 4=Sad, 5=Surprise, 6=Neutral

Data format:
    emotion = int,
    pixels = 48x48 = 1 x 2304,
    Usage = enum(Training,PublicTest,PrivateTest)
    
    <BOF>0,97 25 14...,Training
    ...
    0,97 25 14...,PublicTest
    ...
    0,97 25 14...,PrivateTest<EOF>
'''


EmotionCount = 7
_labels_token = 'labels'
_images_token = 'images'


def load_csv(file_path):
    f = open(file_path, 'r')
    labels, images = [], []

    for line in f:
        label, pixels, usage = line.split(',')
        labels.append(int(label))
        images.append(map(int, pixels.split(' ')))

    f.close()

    np_labels = np.zeros((len(labels), EmotionCount), dtype=np.uint8)
    np_labels[np.arange(len(labels)), labels] = 1
    np_images = np.array(images, dtype=np.uint8)
    return np_images, np_labels


def csv_to_numpy(file_path, output_folder):
    images, labels = load_csv(file_path)

    file_name = os.path.basename(file_path)
    np.save(output_folder + '/%s%s.npy' % (file_name, _labels_token), labels)
    np.save(output_folder + '/%s%s.npy' % (file_name, _images_token), images)


def load_numpy(folder, basename=''):
    # type: (str, str) -> tuple(np.ndarray, np.ndarray)
    label_files = glob.glob1(folder, '*%s*%s.npy' % (basename, _labels_token))
    if len(label_files) > 0:
        np_labels = np.load(folder + '/' + label_files[0])
    else:
        raise Exception('Y file not found.')

    image_files = glob.glob1(folder, '*%s*%s.npy' % (basename, _images_token))
    if len(image_files) > 0:
        np_images = np.load(folder + '/' + image_files[0])
    else:
        raise Exception('X file not found.')

    return np_images, np_labels


def validate_input(x, y):
    if x is None or y is None:
        raise ValueError('Please provide the input.')
    if type(x) is list:
        x = np.array(x)
    if type(y) is list:
        y = np.array(y)
    if type(x) is not np.ndarray or type(y) is not np.ndarray:
        raise ValueError('Please provide the input as %s or %s.' % (list, np.ndarray))
    if x.shape[0] != x.shape[0]:
        raise ValueError('Input is not consistent: x length != y length.')
    return x, y


def load_config(argv):
    argv_dict = parse_argv(argv)
    config_file = argv_dict.get('config-file')
    running_mode = argv_dict.get('running-mode')

    config_dict = parse_config(config_file)
    running_mode_config = config_dict[running_mode]
    return running_mode_config


def parse_argv(argv):
    # type: (type([])) -> dict
    argv_dict = {'flags': []}
    i = 0
    while i < len(argv):
        if argv[i].startswith('-'):
            if argv[i].startswith('--'):
                key = argv[i][2:]
                if i + 1 < len(argv):
                    argv_dict[key] = argv[i + 1]
                    i += 2
                else:
                    print "No value provided for '%s'." % argv[i]
                    exit(-1)
            else:
                argv_dict['flags'].append(argv[i][1:])
                i += 1
        else:
            print "Unrecognized argument '%s'." % argv[i]
            exit(-1)

    return argv_dict


def parse_config(config_file):
    with open(config_file) as cf_file:
        config_dict = yaml.safe_load(cf_file)
    return config_dict


