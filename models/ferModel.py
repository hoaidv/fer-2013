from os import path

import tensorflow as tf
from tensorflow.python.framework.ops import Tensor

from loaders.augment.augmentor import Augmentor
from loaders.augment.uniformAugmentor import UniformAugmentor
from loaders.dataSets.dataSet import DataSet
from loaders.preprocess.centerAndNormalize import CenterAndNormalize
from loaders.preprocess.preprocessor import Preprocessor

GraphsPath = './output/graphs/'
SessionsPath = './output/sessions/'


def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)


def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)


class FerModel:

    def __init__(self, train_set, val_set, model_params, learning_params):
        # type: (DataSet, DataSet, dict, dict) -> FerModel

        # Input data
        self.train_set = train_set
        self.val_set = val_set
        self.x_shape = train_set.x.shape[1:]
        self.num_classes = train_set.y.shape[-1]

        # Read learning params
        self.learning_rate = float(learning_params['learning-rate'])
        self.beta1 = float(learning_params['beta1'])
        self.beta2 = float(learning_params['beta2'])

        # Read model params
        self.dropout1_prob = float(model_params['dropout-prob'])
        self.augmentor = None       # type: Augmentor
        self.preprocessor = None    # type: Preprocessor
        if model_params.get('augmentation'):
            self.augmentor = UniformAugmentor()
        if model_params.get('preprocess'):
            self.preprocessor = CenterAndNormalize()

        # Placeholders for input
        self.xvar = None
        self.yvar = None
        self.dropout1 = None

        # Training operators
        self.loss_op = None
        self.accuracy_op = None
        self.optimizer_op = None
        self.global_step = None

        # Summarization
        self.summary_train_op = None
        self.summary_val_op = None

    def initialize_model(self):
        self.global_step = tf.Variable(0, dtype=tf.int32, trainable=False, name='global_step')

    def train(self, num_epochs, batch_size=128, summary_frequency=50, verbose=True, model_name=None):
        processor = self.preprocessor
        augmentor = self.augmentor

        with tf.Session() as sess:
            writer = tf.summary.FileWriter(GraphsPath, sess.graph)
            sess.run(tf.global_variables_initializer())     # THIS goes FIRST
            saver = None
            if model_name:
                saver = tf.train.Saver()
                save_path = path.join(SessionsPath, model_name)
                checkpoint = tf.train.get_checkpoint_state(path.dirname(save_path))
                if checkpoint and checkpoint.model_checkpoint_path:
                    saver.restore(sess, checkpoint.model_checkpoint_path)   # THIS goes AFTER

            i = 0
            epochs_completed = self.train_set.epochs_completed
            while self.train_set.epochs_completed < num_epochs:
                feed_x, feed_y = self.train_set.next_batch(batch_size, shuffle=True)
                val_x, val_y = self.val_set.x, self.val_set.y

                if augmentor:
                    feed_x, feed_y = augmentor.augment(feed_x, feed_y)

                if processor:
                    processor.reset()
                    processor.initialize(feed_x)
                    feed_x = processor.process(feed_x)
                    val_x = processor.process(val_x)

                sess.run(self.optimizer_op, feed_dict={
                    self.xvar: feed_x,
                    self.yvar: feed_y,
                    self.dropout1: self.dropout1_prob})

                if i % summary_frequency == 0:
                    summary_train, train_accuracy = sess.run(
                        [self.summary_train_op, self.accuracy_op],
                        feed_dict={self.xvar: feed_x, self.yvar: feed_y, self.dropout1: 0})

                    summary_val, val_accuracy = sess.run(
                        [self.summary_val_op, self.accuracy_op],
                        feed_dict={self.xvar: val_x, self.yvar: val_y, self.dropout1: 0})

                    if verbose:
                        print '%d, train-val = %g - %g' % (i, train_accuracy, val_accuracy)

                    writer.add_summary(summary_train, i)
                    writer.add_summary(summary_val, i)

                i += 1
                if self.train_set.epochs_completed > epochs_completed:
                    epochs_completed = self.train_set.epochs_completed
                    if saver and save_path:
                        saver.save(sess=sess, save_path=save_path, global_step=self.global_step)

        writer.close()

    def test(self, x_test, y_test):
        pass
