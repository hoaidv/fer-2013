import math

import tensorflow as tf

from models.ferModel import FerModel, weight_variable, bias_variable
from util.funcs import product


class BkStartModel(FerModel):

    def initialize_model(self):
        FerModel.initialize_model(self)

        self.xvar = tf.placeholder(tf.float32, shape=[None] + list(self.x_shape))
        img_dim = int(math.sqrt(product(self.x_shape)))
        x_spatial = tf.reshape(self.xvar, shape=[-1, img_dim, img_dim, 1])
        self.yvar = tf.placeholder(tf.float32, shape=[None, self.num_classes])

        # Convolution layer 1
        w_conv1 = weight_variable([5, 5, 1, 32])
        b_conv1 = bias_variable([32])

        h_conv1 = tf.nn.conv2d(x_spatial, w_conv1, strides=[1, 1, 1, 1], padding='SAME')
        h_relu1 = tf.nn.relu(h_conv1 + b_conv1)
        h_pool1 = tf.nn.max_pool(
            h_relu1, ksize=[1, 3, 3, 1], strides=[1, 2, 2, 1], padding='SAME')

        # Convolution layer 2
        w_conv2 = weight_variable([4, 4, 32, 32])
        b_conv2 = bias_variable([32])

        h_conv2 = tf.nn.conv2d(h_pool1, w_conv2, strides=[1, 1, 1, 1], padding='SAME')
        h_relu2 = tf.nn.relu(h_conv2 + b_conv2)
        h_pool2 = tf.nn.avg_pool(
            h_relu2, ksize=[1, 3, 3, 1], strides=[1, 2, 2, 1], padding='SAME')

        # Convolution layer 3
        w_conv3 = weight_variable([5, 5, 32, 64])
        b_conv3 = bias_variable([64])

        h_conv3 = tf.nn.conv2d(h_pool2, w_conv3, strides=[1, 1, 1, 1], padding='SAME')
        h_relu3 = tf.nn.relu(h_conv3 + b_conv3)
        h_pool3 = tf.nn.avg_pool(
            h_relu3, ksize=[1, 3, 3, 1], strides=[1, 2, 2, 1], padding='SAME')

        # Dense 1

        # Remember, after all convolution layers, we have
        # h_pool3.shape = (?, width, height, chanels)

        fc1_dim0 = product(h_pool3.shape[1:]).value
        w_fc1 = weight_variable([fc1_dim0, 3072])
        b_fc1 = bias_variable([3072])

        h_pool3_flat = tf.reshape(h_pool3, [-1, fc1_dim0])
        h_fc1 = tf.nn.relu(tf.matmul(h_pool3_flat, w_fc1) + b_fc1)

        # Dropout 1
        self.dropout1 = tf.placeholder(tf.float32)
        h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob=1-self.dropout1)

        # Dense 2
        w_fc2 = weight_variable([3072, self.num_classes])
        b_fc2 = bias_variable([self.num_classes])

        y_conv = tf.matmul(h_fc1_drop, w_fc2) + b_fc2

        # Loss, Accuracy, Optimizer
        logits = tf.nn.softmax_cross_entropy_with_logits(logits=y_conv, labels=self.yvar)
        self.loss_op = tf.reduce_mean(logits)

        optimizer = tf.train.AdamOptimizer(self.learning_rate, self.beta1, self.beta2)
        self.optimizer_op = optimizer.minimize(self.loss_op, global_step=self.global_step)

        predictions = tf.equal(tf.arg_max(y_conv, 1), tf.arg_max(self.yvar, 1))
        self.accuracy_op = tf.reduce_mean(tf.cast(predictions, tf.float32))

        # Summary operator

        self.summary_train_op = tf.summary.merge([
            tf.summary.scalar('train_loss', self.loss_op),
            tf.summary.scalar('train_accuracy', self.accuracy_op)
        ])
        self.summary_val_op = tf.summary.scalar('val_accuracy', self.accuracy_op)
