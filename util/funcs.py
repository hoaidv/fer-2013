

def product(integers):
    return reduce(lambda left, right: left * right, integers)