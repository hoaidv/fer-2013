import numpy as np
import matplotlib.pyplot as plt
import math


def show_images(images, columns=10, color_map='gray'):
    # type: (object, int) -> None
    num_imgs = len(images)
    rows = int(math.ceil(float(num_imgs) / columns))
    fig, axes = plt.subplots(rows, columns)

    for idx, img in enumerate(images):
        i, j = idx / columns, idx % columns
        axes[i, j].imshow(img, cmap=color_map)
        axes[i, j].axis('off')

    plt.show()