import multiprocessing
import sys
import math

from joblib.parallel import Parallel, delayed

from loaders.augment.uniformAugmentor import UniformAugmentor
from loaders.dataSets.simpleDataSet import SimpleDataSet
from loaders.ferLoader import *
from loaders.preprocess.centerAndNormalize import CenterAndNormalize
from models.bkStartModel import BkStartModel
from util.funcs import product
from skimage import img_as_float


def construct_datasets(xtrain, ytrain, xval, yval):
    print 'Convert to float64 images...'
    with Parallel(n_jobs=multiprocessing.cpu_count()) as parallel:
        xtrain_temp = parallel(delayed(img_as_float)(img) for img in xtrain)
        xtrain = np.array(xtrain_temp)

        xval_temp = parallel(delayed(img_as_float)(img) for img in xval)
        xval = np.array(xval_temp)

    print 'Reshape to spatial shape...'
    im_dimension = int(math.sqrt(product(xtrain.shape[1:])))
    xtrain = xtrain.reshape(len(xtrain), im_dimension, im_dimension)
    xval = xval.reshape(len(xval), im_dimension, im_dimension)

    print 'Construct datasets...'
    trainset = SimpleDataSet(xtrain, ytrain)
    valset = SimpleDataSet(xval, yval)
    return trainset, valset


if __name__ == '__main__':

    config = load_config(sys.argv[1:])

    # Required arguments
    x_train, y_train = load_numpy(folder=config.get('train-set'))
    x_val, y_val = load_numpy(folder=config.get('validate-set'))
    epochs = config.get('epochs')
    summary_frequency = config.get('summary-frequency')
    learning_params = config.get('learning-params')
    model_params = config.get('model-params')

    # Optional arguments
    model_name = config.get('model-name')

    train_set, val_set = construct_datasets(x_train, y_train, x_val, y_val)

    print 'Initializing model...'
    bk_start_model = BkStartModel(train_set, val_set, model_params, learning_params)
    bk_start_model.initialize_model()

    print 'Training...'
    bk_start_model.train(
        num_epochs=epochs,
        summary_frequency=summary_frequency,
        model_name=model_name)
